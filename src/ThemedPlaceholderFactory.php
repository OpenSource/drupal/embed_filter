<?php

namespace Drupal\embed_filter;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Template\TwigEnvironment;
use klambt\EmbedFilter\BasePlaceholderFactory;

/**
 * Placeholder factory that uses rendered templates to style the placeholders.
 */
class ThemedPlaceholderFactory extends BasePlaceholderFactory {

  /**
   * The render service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The Twig environment.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twigEnvironment;

  /**
   * Constructs a new ThemedPlaceholderFactory object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   * @param \Drupal\Core\Template\TwigEnvironment $twig_environment
   *   The Twig environment.
   */
  public function __construct(RendererInterface $renderer, TwigEnvironment $twig_environment) {
    $this->renderer = $renderer;
    $this->twigEnvironment = $twig_environment;
  }

  /**
   * {@inheritdoc}
   */
  public function createPlaceholderInner($html_to_hide, $provider = 'unknown', $details = NULL) {
    $render_array = [
      '#theme' => 'embed_filter_placeholder',
      '#provider' => $provider,
      '#details' => $details,
      '#html_to_hide' => $html_to_hide,
      '#cache' => [
        'max_age' => 0,
      ],
    ];

    // Disable twig.debug because of validation issues with the generated comments.
    if ($is_debug = $this->twigEnvironment->isDebug()) {
      $this->twigEnvironment->disableDebug();
    }
    $markup = trim($this->renderer->renderPlain($render_array));
    if ($is_debug) {
      $this->twigEnvironment->enableDebug();
    }

    return (string) $markup;
  }

}
