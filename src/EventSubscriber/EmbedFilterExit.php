<?php

namespace Drupal\embed_filter\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\embed_filter\EmbedFilterService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Apply the embed_filter to rendered markup.
 *
 * @see \Symfony\Component\EventDispatcher\EventSubscriberInterface
 */
class EmbedFilterExit implements EventSubscriberInterface {

  /**
   * The EmbedFilter-service.
   *
   * @var \Drupal\embed_filter\EmbedFilterService
   */
  protected $service;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * EmbedFilterExit constructor.
   */
  public function __construct(EmbedFilterService $service, ConfigFactoryInterface $config_factory, AccountInterface $current_user) {
    $this->service = $service;
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
  }

  /**
   * Applies the filter to the rendered html.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The response-event.
   */
  public function response(FilterResponseEvent $event) {
    $config = $this->configFactory->get('embed_filter.settings');
    if (!$config->get('enabled') || $this->currentUser->hasPermission('bypass embed filter')) {
      return;
    }

    $response = $event->getResponse();

    // @todo Allow other modules to register these?
    $allowed_response_classes = [
      'Drupal\big_pipe\Render\BigPipeResponse',
      'Drupal\Core\Render\HtmlResponse',
    ];

    if (in_array(get_class($response), $allowed_response_classes)) {
      $content = $response->getContent();
      $response->setContent($this->service->getFilter()->setHtml($content)->filterHTML());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::RESPONSE][] = ['response', -10000];

    return $events;
  }

}
