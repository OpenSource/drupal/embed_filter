<?php

namespace Drupal\embed_filter\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Embed filter provider plugin item annotation object.
 *
 * @see \Drupal\embed_filter\Plugin\EmbedFilterProviderManager
 * @see plugin_api
 *
 * @Annotation
 */
class EmbedFilterProvider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The regex patterns for this plugin.
   *
   * @var array
   */
  public $patterns;

}
