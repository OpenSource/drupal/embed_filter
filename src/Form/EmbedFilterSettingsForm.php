<?php

namespace Drupal\embed_filter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for the embed-filter.
 */
class EmbedFilterSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'embed_filter.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'embed_filter_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('embed_filter.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Whether the embed filter is enabled.'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['internal_domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Internal domains'),
      '#description' => $this->t('Enter domains which should not be filtered. One entry per line.'),
      '#default_value' => $config->get('internal_domains'),
    ];

    $form['opt_out'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Opt-out'),
      '#description' => $this->t('Whether the system defaults to allowing all providers.'),
      '#default_value' => $config->get('opt_out'),
    ];

    $form['consolidate_unknown'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Consolidate unknown providers'),
      '#description' => $this->t('Whether all unknown providers should be accepted by simply clicking the agree button.'),
      '#default_value' => $config->get('consolidate_unknown'),
    ];

    $form['allow_dangerous_schemes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow potential dangerous schemes'),
      '#description' => $this->t('Whether all unknown schemes (anything different than http or https) are allowed or not.'),
      '#default_value' => $config->get('allow_dangerous_schemes'),
    ];

    $form['provider_label'] = [
      '#type' => 'details',
      '#title' => $this->t('Provider label'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    foreach (embed_filter_get_providers() as $provider_id => $provider) {
      $form['provider_label'][$provider_id] = [
        '#type' => 'textfield',
        '#title' => $provider->getLabel(),
        '#default_value' => $config->get('provider_label.' . $provider_id),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $config = $this->config('embed_filter.settings');
    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
