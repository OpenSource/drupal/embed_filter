<?php

namespace Drupal\embed_filter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\embed_filter\Plugin\EmbedFilterProviderManagerInterface;
use klambt\EmbedFilter\DefaultConfig;
use klambt\EmbedFilter\Filter;
use klambt\EmbedFilter\ProviderRegistry;

/**
 * Class EmbedFilterService.
 */
class EmbedFilterService {

  /**
   * The filter object.
   *
   * @var \klambt\EmbedFilter\Filter
   */
  protected $filter;

  /**
   * Constructs a new EmbedFilterService object.
   *
   * @param \Drupal\embed_filter\Plugin\EmbedFilterProviderManagerInterface $plugin_manager
   *    The embed filter provider plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   * @param \Drupal\Core\Template\TwigEnvironment $twig_environment
   *   The Twig environment.
   */
  public function __construct(EmbedFilterProviderManagerInterface $plugin_manager, ConfigFactoryInterface $config_factory, RendererInterface $renderer, TwigEnvironment $twig_environment) {
    // Build a custom filter configuration based on module settings.
    $filter_config = new DefaultConfig();
    $settings = $config_factory->get('embed_filter.settings');

    $filter_config->setAllowDangerousSchemes($settings->get('allow_dangerous_schemes'));
    if ($internal_domains = $settings->get('internal_domains')) {
      $filter_config->setInternalDomains(array_map('trim', explode(PHP_EOL, $internal_domains)));
    }

    // Reset the provider registry and populate it with discovered plugins.
    $provider_registry = new ProviderRegistry();
    foreach ($plugin_manager->getProviders() as $id => $provider) {
      $provider_registry->addProvider($provider->getProviderObject());
    }
    $filter_config->setProviderRegistry($provider_registry);

    // Use themed placeholders instead of the default ones.
    $filter_config->setPlaceholderFactory(new ThemedPlaceholderFactory($renderer, $twig_environment));

    // Create and store the filter using the above configuration.
    $this->filter = new Filter($filter_config);
  }

  /**
   * Gets the current filter object.
   *
   * @return \klambt\EmbedFilter\Filter
   *   The filter object.
   */
  public function getFilter() {
    return $this->filter;
  }

}
