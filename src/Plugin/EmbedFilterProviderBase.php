<?php

namespace Drupal\embed_filter\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Embed filter provider plugin plugins.
 */
abstract class EmbedFilterProviderBase extends PluginBase implements EmbedFilterProviderInterface {

  public function getId() {
    return $this->getPluginId();
  }

  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  public function getProviderClassName() {
    return $this->pluginDefinition['provider_class_name'];
  }

  public function getProviderObject() {
    $class_name = $this->getProviderClassName();
    return new $class_name();
  }

}
