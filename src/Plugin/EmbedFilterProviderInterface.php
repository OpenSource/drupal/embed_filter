<?php

namespace Drupal\embed_filter\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Embed filter provider plugin plugins.
 */
interface EmbedFilterProviderInterface extends PluginInspectionInterface {

  public function getId();

  public function getLabel();

  public function getProviderClassName();

  public function getProviderObject();

}
