<?php

namespace Drupal\embed_filter\Plugin\EmbedFilterProvider;

use Drupal\embed_filter\Plugin\EmbedFilterProviderBase;

/**
 * Class Pintrest.
 *
 * @EmbedFilterProvider(
 *   id = "pinterest",
 *   label = @Translation("Pinterest"),
 *   provider_class_name = "\klambt\EmbedFilter\Providers\Pinterest"
 * )
 */
class Pinterest extends EmbedFilterProviderBase {

}
