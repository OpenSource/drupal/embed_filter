<?php

namespace Drupal\embed_filter\Plugin\EmbedFilterProvider;

use Drupal\embed_filter\Plugin\EmbedFilterProviderBase;

/**
 * Class Facebook.
 *
 * @EmbedFilterProvider(
 *   id = "facebook",
 *   label = @Translation("Facebook"),
 *   provider_class_name = "\klambt\EmbedFilter\Providers\Facebook"
 * )
 */
class Facebook extends EmbedFilterProviderBase {

}
