<?php

namespace Drupal\embed_filter\Plugin\EmbedFilterProvider;

use Drupal\embed_filter\Plugin\EmbedFilterProviderBase;

/**
 * Class Instagram.
 *
 * @EmbedFilterProvider(
 *   id = "instagram",
 *   label = @Translation("Instagram"),
 *   provider_class_name = "\klambt\EmbedFilter\Providers\Instagram"
 * )
 */
class Instagram extends EmbedFilterProviderBase {

}
