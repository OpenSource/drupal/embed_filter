<?php
namespace Drupal\embed_filter\Plugin\EmbedFilterProvider;

use Drupal\embed_filter\Plugin\EmbedFilterProviderBase;

/**
 * Class Giphy.
 *
 * @EmbedFilterProvider(
 *   id = "giphy",
 *   label = @Translation("Giphy"),
 *   provider_class_name = "\klambt\EmbedFilter\Providers\Giphy"
 * )
 */
class Giphy extends EmbedFilterProviderBase
{

}
