<?php

namespace Drupal\embed_filter\Plugin\EmbedFilterProvider;

use Drupal\embed_filter\Plugin\EmbedFilterProviderBase;

/**
 * Class Youtube.
 *
 * @EmbedFilterProvider(
 *   id = "youtube",
 *   label = @Translation("YouTube"),
 *   provider_class_name = "\klambt\EmbedFilter\Providers\YouTube"
 * )
 */
class Youtube extends EmbedFilterProviderBase {

}
