<?php

namespace Drupal\embed_filter\Plugin\EmbedFilterProvider;

use Drupal\embed_filter\Plugin\EmbedFilterProviderBase;

/**
 * Class Twitter.
 *
 * @EmbedFilterProvider(
 *   id = "twitter",
 *   label = @Translation("Twitter"),
 *   provider_class_name = "\klambt\EmbedFilter\Providers\Twitter"
 * )
 */
class Twitter extends EmbedFilterProviderBase {

}
