<?php

namespace Drupal\embed_filter\Plugin\EmbedFilterProvider;

use Drupal\embed_filter\Plugin\EmbedFilterProviderBase;

/**
 * Class Vimeo.
 *
 * @EmbedFilterProvider(
 *   id = "vimeo",
 *   label = @Translation("Vimeo"),
 *   provider_class_name = "\klambt\EmbedFilter\Providers\Vimeo"
 * )
 */
class Vimeo extends EmbedFilterProviderBase {

}
