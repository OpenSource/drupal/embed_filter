<?php

namespace Drupal\embed_filter\Plugin;

interface EmbedFilterProviderManagerInterface {

  /**
   * Get the providers as EmbedFilterProvider plugin instances.
   *
   * @return \Drupal\embed_filter\Plugin\EmbedFilterProviderInterface[]
   *   The provider objects.
   */
  public function getProviders();

}
