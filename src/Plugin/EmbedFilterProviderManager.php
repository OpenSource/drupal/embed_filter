<?php

namespace Drupal\embed_filter\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Embed filter provider plugin plugin manager.
 */
class EmbedFilterProviderManager extends DefaultPluginManager implements EmbedFilterProviderManagerInterface {

  /**
   * Constructs a new EmbedFilterProviderManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EmbedFilterProvider', $namespaces, $module_handler, 'Drupal\embed_filter\Plugin\EmbedFilterProviderInterface', 'Drupal\embed_filter\Annotation\EmbedFilterProvider');
    $this->alterInfo('embed_filter_embed_filter_provider_plugin_info');
    $this->setCacheBackend($cache_backend, 'embed_filter_embed_filter_provider_plugin_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getProviders() {
    $providers = [];
    foreach ($this->getDefinitions() as $id => $definition) {
      $providers[$id] = $this->createInstance($id);
    }
    return $providers;
  }

}
