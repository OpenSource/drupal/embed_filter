(function ($) {
    'use strict';

    $(document).ready(function() {
        var container = $('.embed-filter-popup__container');
        var popupContainer = $('#embed-filter-popup');

        $('.on-off-switch').click(function () {
            $(this).find('.on-off-switch__checkbox').trigger('click');
        });

        $('.embed_filter_options_toggle').click(function () {
            var toggle = $(this);
            if(container.is(':visible')) {
                container.animate({ height: 0 }, 400, null, function () {
                    $(this).hide();
                    toggle.addClass('collapsed');
                });
            } else {
                container.show().css({ height: 'auto' });
                var height = container.outerHeight();
                container.css({ height: 0 }).animate({ height: height }, 400, null, function () {
                    $(this).css({ height: 'auto' });
                    toggle.removeClass('collapsed');
                });
            }
        });

      $('.embed-filter-popup__toggle').click(function () {
        var height = popupContainer.outerHeight();

        popupContainer.show()
          .addClass('embed-filter-popup--bottom')
          .css({ bottom: -1 * height })
          .animate({ bottom: 0 }, 600, null, function () {
          });

        $(this).addClass('embed-filter-popup__toggle--hidden');
      });
    });

})(jQuery);