(function ($) {
    'use strict';

    $(document).ready(function() {

        var cookieName = 'cookie_agreement';

        try {
            var value = localStorage.getItem(cookieName);
                value = parseInt(value);
        } catch {
            var value = 0;
        }

        var popup = $('.embed-filter-popup');

        if (!isNaN(value) && value == 1) {
            popup.hide();
        } else {
            popup.show()
                .addClass('embed-filter-popup--bottom')
                .css({ bottom: -1 * popup.outerHeight() })
                .animate({ bottom: 0 }, 800, null, function () {
            });
            $('.embed-filter-popup__toggle').addClass('embed-filter-popup__toggle--hidden');
        }

        $('.embed-filter-popup--cookie-ok').click(function () {
            localStorage.setItem(cookieName,'1');
            popup.hide();
            $('.embed-filter-popup__toggle').removeClass('embed-filter-popup__toggle--hidden');
        });

    });

})(jQuery);