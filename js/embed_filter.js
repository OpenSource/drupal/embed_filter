/**
 * @file embed_filter.js
 *
 * Defines the behavior of the embed filter banner.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.embedFilterPopup = {
    attach: function (context) {
      if (typeof drupalSettings.embed_filter_providers === 'undefined') {
        return;
      }

      // Necessary when the filter is used in conjunction with infite scroll to prevent multiple initializations
      if(this.is_initialized) {
        if (typeof Drupal.behaviors.embedFilterPopup.embedFilterHandler != 'undefined') {
          Drupal.behaviors.embedFilterPopup.embedFilterHandler.executeRenderAll();

          // Finally, regardless of whether a pop-up was shown, we allow people to
          // click on a link in the placeholders to allow them to be replaced.
          $('.embed-filter-placeholder .embed-filter-placeholder__link').once('embed-filter-placeholder-link').each(function () {
            const providerName = this.dataset['provider'];
            $(this).on('click', function() {
              Drupal.behaviors.embedFilterPopup.embedFilterHandler.allowProvider(providerName);
              Drupal.behaviors.embedFilterPopup.embedFilterHandler.writeStorage();
              Drupal.behaviors.embedFilterPopup.embedFilterHandler.executeRenderAll();
              $('.on-off-switch__checkbox[value="'+providerName+'"]').prop('checked', true);
            });
          });

          // Register Allow all providers button
          $('.embed-filter-placeholder .embed-filter-placeholder__cookie-ok').on('click', function () {
            Drupal.behaviors.embedFilterPopup.embedFilterHandler.resetStorage();
            Drupal.behaviors.embedFilterPopup.embedFilterHandler.allowProviders(providerNames);
            Drupal.behaviors.embedFilterPopup.embedFilterHandler.writeStorage();
            Drupal.behaviors.embedFilterPopup.embedFilterHandler.executeRenderAll();
            $('.on-off-switch__checkbox').prop('checked', true);
            // Hide the popup
            $('#embed-filter-popup').hide();
          });

          return;
        }
      }

      // For more readable code.
      const consolidate_unknown = drupalSettings.embed_filter_consolidate_unknown;

      // Scan for:
      // - Known providers that haven't been allowed or rejected yet.
      let discoveredKnown = [];
      // - Unknown providers that haven't been allowed or rejected yet.
      let discoveredUnknown = [];

      // Instantiate the handler and load the providers from storage (if any).
      const embedFilterHandler = new EmbedFilterHandler('ins', 'embed_filter_options');
      Drupal.behaviors.embedFilterPopup.embedFilterHandler  = embedFilterHandler;

      embedFilterHandler.loadFromStorage();

      // Check whether all configured providers have been shown for approval
      // before and add them to the handler. Also keep track of the provider
      // names.
      let providerNames = [];
      for (let i = 0; i < drupalSettings.embed_filter_providers.length; i++) {
        let providerName = drupalSettings.embed_filter_providers[i].id;
        providerNames.push(providerName);

        if (!embedFilterHandler.hasProvider(providerName)) {
          discoveredKnown.push(providerName);

          // When using an opt-out, we allow this provider by default.
          if (drupalSettings.embed_filter_opt_out) {
            embedFilterHandler.allowProvider(providerName);
          }
          else {
            embedFilterHandler.rejectProvider(providerName);
          }
        }
      }

      // Select the checkboxes of the allowed providers.
      Drupal.behaviors.embedFilterPopup.initFilterOptions('.embed-filter-popup__options', drupalSettings.embed_filter_providers, embedFilterHandler.getAllowedProviders());

      // Render those providers that are allowed already.
      embedFilterHandler.executeRenderAll();

      // Scan the DOM for non-rendered provider placeholders.
      let processedIndex = [];
      $('ins[data-provider]').once('embed-filter-placeholder').each(function () {
        const providerName = this.dataset['provider'];

        // Scan for each provider only once.
        if (processedIndex.indexOf(providerName) === -1) {
          processedIndex.push(providerName);

          // If the provider is not in the handler at this stage, we can be sure
          // it's an unknown provider because all configured providers were
          // added to the handler right after we loaded from storage.
          if (!embedFilterHandler.hasProvider(providerName)) {
            const providerDetails = JSON.parse(this.dataset['html'])['details'];
            discoveredUnknown.push({"id" : providerName, "label" : providerDetails});

            // If we are consolidating the unknown providers, we can already
            // register them as allowed. This will save them to the storage when
            // the user clicks on the "I agree" button.
            if (consolidate_unknown) {
              embedFilterHandler.allowProvider(providerName);
            }
          }
        }
      });

      // If the user had already consented to certain providers before while the
      // consolidation toggle was turned on, we have their approval for allowing
      // "any provider not listed here", meaning we can safely allow new
      // providers discovered above without having to show the pop-up.
      if (embedFilterHandler.hasStorage() && consolidate_unknown && (discoveredKnown.length > 0 || discoveredUnknown.length > 0)) {
        embedFilterHandler.allowProviders(discoveredKnown);
        embedFilterHandler.writeStorage();
        embedFilterHandler.executeRenderAll();
      }

      // As a follow-up to the above logic, we only show the pop-up if a new
      // provider was detected and we are not consolidating unknown providers or
      // if the user had not accepted the pop-up yet.
      const showPopup = !embedFilterHandler.hasStorage() || (!consolidate_unknown && (discoveredKnown.length > 0 || discoveredUnknown.length > 0));
      //const showPopup = true; // For demo purposes only.

      if (showPopup) {
        // @todo We could highlight the new known providers here.

        // Create checkboxes for unknown providers and show the container, but
        // only if we are asking for approval on a per unknown provider basis.
        if (!consolidate_unknown && discoveredUnknown.length > 0) {
          Drupal.behaviors.embedFilterPopup.initFilterOptions('.embed-filter-popup__unknown-options', discoveredUnknown, embedFilterHandler.getAllowedProviders());
          $('.embed-filter-popup__unknown-container').show();
        }

        const popupContainer = $('#embed-filter-popup');
        const height = popupContainer.outerHeight();

        // Animate the pop-up container.
        popupContainer.show()
          .addClass('embed-filter-popup--bottom')
          .css({ bottom: -1 * height })
          .animate({ bottom: 0 }, 1000, null, function () {
          });

        // Register the submit button.
        popupContainer.find('.embed-filter-popup--ok').on('click', function() {
          // First update the allowed and rejected providers.
          popupContainer.find('.on-off-switch__checkbox').each(function() {
            if ($(this).is(':checked')) {
              embedFilterHandler.allowProvider(this.value);
            }
            else {
              embedFilterHandler.rejectProvider(this.value);
            }
          });

          // Then store the updated provider selection and render all approved
          // providers again so newly selected ones are rendered as well.
          embedFilterHandler.writeStorage();
          embedFilterHandler.executeRenderAll();

          // Hide the popup
          popupContainer.hide();
          $('.embed-filter-popup__toggle').removeClass('embed-filter-popup__toggle--hidden');
        });

        // Register the reset button.
        popupContainer.find('.embed-filter-popup--reset').on('click', function(){

          // Reset the filter as to the factory default
          embedFilterHandler.resetStorage();

          if (drupalSettings.embed_filter_opt_out) {
            embedFilterHandler.allowProviders(providerNames);
            $('.on-off-switch__checkbox').prop('checked', true);
          }
          else {
            embedFilterHandler.rejectProviders(providerNames);
            $('.on-off-switch__checkbox').prop('checked', false);
          }

          embedFilterHandler.writeStorage();
          embedFilterHandler.executeRenderAll();

          // Hide the popup
          popupContainer.hide();
          $('.embed-filter-popup__toggle').removeClass('embed-filter-popup__toggle--hidden');
        });

        // Register Allow all providers button
        popupContainer.find('.embed-filter-popup--cookie-ok').on('click', function () {
          embedFilterHandler.resetStorage();
          embedFilterHandler.allowProviders(providerNames);
          embedFilterHandler.writeStorage();
          embedFilterHandler.executeRenderAll();
          $('.on-off-switch__checkbox').prop('checked', true);
          // Hide the popup
          popupContainer.hide();
        });

      }

      // Finally, regardless of whether a pop-up was shown, we allow people to
      // click on a link in the placeholders to allow them to be replaced.
      $('.embed-filter-placeholder__link').once('embed-filter-placeholder-link').each(function () {
        const providerName = this.dataset['provider'];
        $(this).on('click', function() {
          embedFilterHandler.allowProvider(providerName);
          embedFilterHandler.writeStorage();
          embedFilterHandler.executeRenderAll();

          $('.on-off-switch__checkbox[value="'+providerName+'"]').prop('checked', true);
        });
      });
    },
    initFilterOptions: function(wrapperSelector, options, approved) {
      let optionHTML = $('#embed-filter-popup-option-template').html();
      let optionWrapper = $(wrapperSelector);

      $.each(options, function (i, option) {
        let fragment;

        // Newer browsers support the template element, which inherently uses a
        // document fragment for its content property.
        let template = document.createElement('template');
        if ('content' in template) {
          template.innerHTML = optionHTML;
          fragment = template.content;
        }
        // Older browsers instead require us to create and populate our own document
        // fragment using any old element such as a div and retrieving its children
        // one by one.
        else {
          fragment = document.createDocumentFragment();
          const div = document.createElement('div');
          div.innerHTML = optionHTML;
          while (div.firstChild) {
            fragment.appendChild(div.firstChild);
          }
        }

        let optionId = 'embed-filter-option--' + option.id;
        let optionLabel = $('.embed-filter-popup__option-label', fragment);
        let optionInput = $('.on-off-switch__checkbox', fragment);
        optionLabel.text(option.label);
        optionLabel.attr('for', optionId);
        optionInput.val(option.id);
        optionInput.attr('id', optionId);
        optionInput.prop('checked', approved.indexOf(option.id) !== -1);

        optionWrapper.append(fragment);
      });

      this.is_initialized = true;
    }
  };

})(jQuery, Drupal, drupalSettings);
