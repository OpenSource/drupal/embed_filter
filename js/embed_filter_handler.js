var EmbedFilterHandler = function(tag, storageName, defaultProviders, optOut) {
  // @todo Could be inlined as only used twice. Discuss with Stephan.
  const dataProvider = 'provider';
  const dataHtml = 'html';
  const htmlProperty = 'html';

  let allowedProviders = [];
  let rejectedProviders = [];

  this.loadFromStorage = function () {
    if (this.hasStorage()) {
      const storage = JSON.parse(localStorage.getItem(storageName));
      allowedProviders = storage['allowed'];
      rejectedProviders = storage['rejected'];
    }
  };

  this.hasStorage = function () {
    return localStorage.getItem(storageName);
  };

  this.writeStorage = function () {
    localStorage.setItem(storageName, JSON.stringify({'allowed' : allowedProviders, 'rejected' : rejectedProviders}));
  };

  this.resetStorage = function() {
    this.setAllowedProviders([]);
    this.setRejectedProviders([]);
    this.writeStorage();
  }

  this.hasProvider = function (providerName) {
    return allowedProviders.indexOf(providerName) !== -1 || rejectedProviders.indexOf(providerName) !== -1;
  };

  this.allowProvider = function (providerName) {
    const allowedIndex = allowedProviders.indexOf(providerName);
    const rejectedIndex = rejectedProviders.indexOf(providerName);
    if (allowedIndex === -1) {
      allowedProviders.push(providerName);
    }
    if (rejectedIndex !== -1) {
      rejectedProviders.splice(rejectedIndex, 1);
    }
  };

  this.allowProviders = function (providerNames) {
    for (let i = 0; i < providerNames.length; i++) {
      this.allowProvider(providerNames[i]);
    }
  };

  this.rejectProvider = function (providerName) {
    const allowedIndex = allowedProviders.indexOf(providerName);
    const rejectedIndex = rejectedProviders.indexOf(providerName);
    if (rejectedIndex === -1) {
      rejectedProviders.push(providerName);
    }
    if (allowedIndex !== -1) {
      allowedProviders.splice(allowedIndex, 1);
    }
  };

  this.rejectProviders = function (providerNames) {
    for (let i = 0; i < providerNames.length; i++) {
      this.rejectProvider(providerNames[i]);
    }
  };

  this.getAllowedProviders = function () {
    return allowedProviders.slice();
  };

  this.setAllowedProviders = function (providerNames) {
    allowedProviders = providerNames.slice();
  };

  this.getRejectedProviders = function () {
    return rejectedProviders.slice();
  };

  this.setRejectedProviders = function (providerNames) {
    rejectedProviders = providerNames.slice();
  };

  this.replacePlaceholder = function(el) {
    const html = JSON.parse(el.dataset[dataHtml])[htmlProperty].trim();

    // Newer browsers support the template element, which inherently uses a
    // document fragment for its content property.
    const template = document.createElement('template');
    if ('content' in template) {
      template.innerHTML = html;
      el.parentNode.replaceChild(template.content, el);
    }
    // Older browsers instead require us to create and populate our own document
    // fragment using any old element such as a div and retrieving its children
    // one by one.
    else {
      const frag = document.createDocumentFragment();
      const div = document.createElement('div');
      div.innerHTML = html;
      while (div.firstChild) {
        frag.appendChild(div.firstChild);
      }
      el.parentNode.replaceChild(frag, el);
    }
  };

  this.executeRenderProvider = function(providerName) {
    const selector = tag + '[data-' + dataProvider + '="' + providerName + '"]';
    const ins = document.querySelectorAll(selector);
    for (let i = 0; i < ins.length; ++i) {
      this.replacePlaceholder(ins[i]);
    }
  };

  this.executeRenderAll = function() {
    if (allowedProviders.length) {
      for (let i = 0; i < allowedProviders.length; i++) {
        this.executeRenderProvider(allowedProviders[i]);
      }
    }
  };

};
